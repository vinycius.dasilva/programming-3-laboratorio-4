using Avalonia;
using Avalonia.Controls;

namespace Laboratorio4Avalonia.ViewModels
{
    // Struct representing the properties of a spaceship in the application.
    public struct SSpaceShip
    {
        // Constructor for initializing the spaceship properties.
        public SSpaceShip(Image sourceImage, Size size, int x, int y)
        {
            SourceImage = sourceImage;
            ImageSize = size;
            X = x;
            Y = y;
        }

        // Property representing the image of the spaceship.
        public Image SourceImage { get; }

        // Property representing the size of the spaceship image.
        public Size ImageSize { get; }

        // Property representing the X coordinate of the spaceship position.
        public int X { get; set; }

        // Property representing the Y coordinate of the spaceship position.
        public int Y { get; set; }
    }
}