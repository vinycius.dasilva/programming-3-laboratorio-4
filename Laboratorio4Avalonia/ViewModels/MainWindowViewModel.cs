﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using NAudio.Wave;

namespace Laboratorio4Avalonia.ViewModels
{
    // The main view model for the application.
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        // Size of the space in the application.
        private static readonly Size SpaceSize = new(48, 48);

        // Audio file reader for playing sounds.
        private AudioFileReader _audioFileReader;

        // Sprite representing the spaceship in the application.
        private SSpaceShip _spaceShipSprite;

        // Interface for playing audio.
        private IWavePlayer _wavePlayer;

        // Constructor for the view model.
        public MainWindowViewModel()
        {
            // Initialize the spaceship sprite with default values.
            _spaceShipSprite = new SSpaceShip(SpaceShipImage, new Size(SpaceSize.Width, SpaceSize.Height),
                (int)(ScreenWidth - SpaceSize.Width) / 2, 100);
        }

        // Image representing the spaceship loaded from a URI.
        private Image SpaceShipImage { get; } = new()
        {
            Source = new Bitmap(AssetLoader.Open(new Uri("avares://Laboratorio4Avalonia/Assets/space-ship.png")))
        };

        // Image representing the background loaded from a URI.
        public Image BackgroundImage { get; } = new()
        {
            Source = new Bitmap(AssetLoader.Open(new Uri("avares://Laboratorio4Avalonia/Assets/background-image.png")))
        };

        // Default move distance for the spaceship.
        public int DefaultDeltaMove => 10;

        // Width of the application screen.
        public int ScreenWidth => 1600;

        // Height of the application screen.
        public int ScreenHeight => 900;

        // Property representing the spaceship sprite with property changed notification.
        public SSpaceShip SpaceShipSprite
        {
            get => _spaceShipSprite;
            private set
            {
                _spaceShipSprite = value;
                OnPropertyChanged();
            }
        }

        // Event for property changed notifications.
        public event PropertyChangedEventHandler? PropertyChanged;

        // Method for playing audio from an asset.
        private void PlayAudioFromAsset(string assetName)
        {
            // Open the asset stream.
            using var stream = AssetLoader.Open(new Uri($"avares://Laboratorio4Avalonia/Assets/{assetName}"));
            if (stream == null)
                throw new InvalidOperationException("Resource not found.");

            // Create a temporary file and copy the asset stream into it.
            var tempFile = Path.GetTempFileName();
            using (var fileStream = File.Create(tempFile))
            {
                stream.CopyTo(fileStream);
            }

            // Initialize and play the audio file using NAudio.
            _wavePlayer = new WaveOutEvent();
            _audioFileReader = new AudioFileReader(tempFile);
            _wavePlayer.Init(_audioFileReader);
            _wavePlayer.Play();

            // Cleanup after playback is complete.
            _wavePlayer.PlaybackStopped += (sender, args) =>
            {
                _audioFileReader.Dispose();
                _wavePlayer.Dispose();
                File.Delete(tempFile);
            };
        }

        // Method for handling property changed notifications.
        private void OnPropertyChanged([CallerMemberName] string? name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // Method for moving the spaceship within the application space.
        public void MoveSpaceShip(int deltaX, int deltaY)
        {
            // Create a new spaceship sprite with the same properties as the current one.
            var newSpace = new SSpaceShip(_spaceShipSprite.SourceImage, _spaceShipSprite.ImageSize, _spaceShipSprite.X,
                _spaceShipSprite.Y);

            // Check if the new position is within the application screen boundaries.
            if (newSpace.X + deltaX >= 0 && newSpace.X + deltaX + SpaceSize.Width <= ScreenWidth)
            {
                newSpace.X += deltaX;
            }

            if (newSpace.Y + deltaY >= 0 && newSpace.Y + deltaY + SpaceSize.Height <= ScreenHeight)
            {
                newSpace.Y += deltaY;
            }

            // Play a sound indicating spaceship movement.
            PlayAudioFromAsset("space-movement.mp3");

            // Update the spaceship sprite with the new position.
            SpaceShipSprite = newSpace;
        }
    }
}
