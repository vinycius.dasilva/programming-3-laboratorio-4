using Avalonia.Controls;
using Avalonia.Input;
using Laboratorio4Avalonia.ViewModels;

namespace Laboratorio4Avalonia.Views
{
    // Partial class representing the main window of the application.
    public partial class MainWindow : Window
    {
        // Constructor for the main window.
        public MainWindow()
        {
            // Initialize the window components and set the DataContext to a new instance of the view model.
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        // Event handler for key down events in the window.
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            // Retrieve the view model from the DataContext.
            var viewModel = DataContext as MainWindowViewModel;
            if (viewModel == null) return;

            // Set the default movement distance from the view model.
            var defaultDelta = viewModel.DefaultDeltaMove;

            // Switch statement to handle different key presses and invoke spaceship movement accordingly.
            switch (e.Key)
            {
                // Up
                case Key.W:
                    viewModel.MoveSpaceShip(0, defaultDelta);
                    break;
                // Down
                case Key.S:
                    viewModel.MoveSpaceShip(0, -defaultDelta);
                    break;
                // Left
                case Key.A:
                    viewModel.MoveSpaceShip(-defaultDelta, 0);
                    break;
                // Right
                case Key.D:
                    viewModel.MoveSpaceShip(defaultDelta, 0);
                    break;

                case Key.Up:
                    viewModel.MoveSpaceShip(0, defaultDelta);
                    break;
                case Key.Down:
                    viewModel.MoveSpaceShip(0, -defaultDelta);
                    break;
                case Key.Left:
                    viewModel.MoveSpaceShip(-defaultDelta, 0);
                    break;
                case Key.Right:
                    viewModel.MoveSpaceShip(defaultDelta, 0);
                    break;
            }
        }
    }
}
